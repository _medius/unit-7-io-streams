package com.epam.four;

import java.io.Serializable;

public class SecondLevel implements Serializable {

	private final ThirdLevel thirdLevel;

	public SecondLevel(ThirdLevel thirdLevel) {
		this.thirdLevel = thirdLevel;
	}

	public ThirdLevel getThirdLevel() {
		return thirdLevel;
	}
}
