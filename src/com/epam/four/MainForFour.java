package com.epam.four;

import java.io.*;

public class MainForFour {

	private static final String PATH_TO_FILE_ONE = "C:\\Users\\Medius\\Desktop\\objectOne.dat";
	private static final String PATH_TO_FILE_TWO = "C:\\Users\\Medius\\Desktop\\objectTwo.dat";

	private File filePathOne = new File(PATH_TO_FILE_ONE);
	private File filePathTwo = new File(PATH_TO_FILE_TWO);

	private ThirdLevel thirdLevel = new ThirdLevel();
	private SecondLevel secondLevel = new SecondLevel(thirdLevel);

	private FirstLevel objectToWriteOne = new FirstLevel(secondLevel);
	private FirstLevel objectToWriteTwo = new FirstLevel(secondLevel);

	private FirstLevel objectReadOne;
	private FirstLevel objectReadTwo;

	private void compareObjects(FirstLevel objectOne, FirstLevel objectTwo) {
		System.out.println("Compare first level of objects, expects false.");
		System.out.println(objectOne == objectTwo);

		System.out.println("Compare second level of objects, expects true.");
		System.out.println(objectOne.getSecondLevel() == objectTwo.getSecondLevel());

		System.out.println("Compare third level of objects, expects true.");
		System.out
			.println(objectOne.getSecondLevel().getThirdLevel() == objectTwo.getSecondLevel().getThirdLevel());
	}

	private FirstLevel readObject(File filePath) {
		FirstLevel objectRead = null;
		try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(filePath))) {
			objectRead = (FirstLevel) objectInputStream.readObject();
			System.out.println("Has been read " + filePath.getName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return objectRead;
	}

	private void writeObject(File filePath, FirstLevel objectToWrite) {
		try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(filePath))) {
			objectOutputStream.writeObject(objectToWrite);
			System.out.println("Has been written " + filePath.getName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void appStart() {
		System.out.println("\nCompare original objects:");
		compareObjects(objectToWriteOne, objectToWriteTwo);

		writeObject(filePathOne, objectToWriteOne);
		writeObject(filePathTwo, objectToWriteTwo);

		objectReadOne = readObject(filePathOne);
		objectReadTwo = readObject(filePathTwo);

		System.out.println("\nCompare objects has been read:");
		compareObjects(objectReadOne, objectReadTwo);
	}

	public static void main(String[] args) {
		new MainForFour().appStart();
	}
}
