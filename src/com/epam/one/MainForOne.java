package com.epam.one;

public class MainForOne {
	private static final String PATH_TO_FILE = "C:\\Users\\Medius\\Desktop\\IOTasksFile.txt";

	private void firstTask() {
		System.out.println("First task output.");
		ReverseReader reverseReader = new ReverseReader(PATH_TO_FILE);
		reverseReader.readFile();
	}

	private void appStart() {
		firstTask();
	}

	public static void main(String[] args) {
		new MainForOne().appStart();
	}
}
