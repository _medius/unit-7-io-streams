package com.epam.three;

import java.io.Serializable;

public class Product implements Serializable {

	private final String title;
	private final float price;
	private final transient int amount;

	public Product(String title, float price, int amount) {
		this.title = title;
		this.price = price;
		this.amount = amount;
	}

	public String getTitle() {
		return title;
	}

	public float getPrice() {
		return price;
	}

	public int getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return "Product{" +
				"title='" + title + '\'' +
				", price=" + price +
				", amount=" + amount +
				'}';
	}
}
