package com.epam.five;

public class MainForFive {

	private void appStart() {
		MyFileReader fileReader = new MyFileReader();
		fileReader.readFile();
	}

	public static void main(String[] args) {
		new MainForFive().appStart();
	}
}
