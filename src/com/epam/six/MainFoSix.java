package com.epam.six;

import com.epam.three.Product;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainFoSix {

	private static final String SERIALIZE_PATH = "C:\\Users\\Medius\\Desktop\\";

	private List<Product> originalProducts = new ArrayList<>();
	private List<File> paths;
	private List<Product> readProducts;

	private void createData() {
		originalProducts.add(new Product("coat", 103.5f, 10));
		originalProducts.add(new Product("chair", 252.1f, 5));
		originalProducts.add(new Product("wrench", 25.3f, 3));
	}

	private void sixTask() {
		System.out.println("Products to serialize:");
		originalProducts.forEach(System.out::println);

		paths = new ProductWriter().serializeProducts(originalProducts, SERIALIZE_PATH);

		readProducts = new ProductReader().deserializeProducts(paths);

		System.out.println("Products after deserialize:");
		readProducts.forEach(System.out::println);
	}

	private void appStart() {
		createData();
		sixTask();
	}

	public static void main(String[] args) {
		new MainFoSix().appStart();
	}
}
