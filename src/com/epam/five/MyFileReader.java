package com.epam.five;

import java.io.*;

public class MyFileReader {

	private BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
	private File filePath;

	public void readFile() {
		String path;
		boolean readSuccessfully = false;
		path = keyboard("Please enter path to file. ");

		while (!readSuccessfully) {
			File filePath = new File(path);

			try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {

				String line;
				while ((line = bufferedReader.readLine()) != null) {
					keyboard("Press \"ENTER\" to read next line...");
					System.out.println(line);
				}
				System.out.println("End of file.");
				readSuccessfully = true;

			} catch (FileNotFoundException e) {
				System.out.println("This file not found.");
				path = keyboard("Please choose another file. ");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		closeConsoleReader();
	}

	private String keyboard(String message) {
		String inputLine = null;
		System.out.print(message);
		try {
			inputLine = consoleReader.readLine();
			if (inputLine.length() == 0) {
				return "";
			}
		} catch (IOException e) {
			System.out.println("IOException: " + e);
		}
		return inputLine;
	}

	private void closeConsoleReader() {
		try {
			consoleReader.close();
			System.out.println("All streams has been closed.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
