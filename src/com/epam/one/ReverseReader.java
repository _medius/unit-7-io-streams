package com.epam.one;

import java.io.*;
import java.util.*;

public class ReverseReader {

	private final String path;

	public ReverseReader(String path) {
		this.path = path;
	}

	public void readFile() {
		File filePath = new File(path);
		List<Character> characters = new ArrayList<>();

		try (FileReader fileReader = new FileReader(filePath)) {
			int ch;
			while ((ch = fileReader.read()) != -1) {
				characters.add((char) ch);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Collections.reverse(characters);
		characters.forEach(System.out::print);
	}
}
