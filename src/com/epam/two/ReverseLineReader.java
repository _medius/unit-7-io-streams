package com.epam.two;

import java.io.*;
import java.util.*;

public class ReverseLineReader {

	private final String path;

	public ReverseLineReader(String path) {
		this.path = path;
	}

	public void readLIneFile() {
		File filePath = new File(path);
		List<String> lines = new ArrayList<>();

		try(BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
			String line;
			while ((line = bufferedReader.readLine()) != null)
			lines.add(line);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Collections.reverse(lines);
		lines.forEach(System.out::println);
	}
}
