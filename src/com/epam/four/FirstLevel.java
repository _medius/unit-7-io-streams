package com.epam.four;

import java.io.Serializable;

public class FirstLevel implements Serializable {

	private final SecondLevel secondLevel;

	public FirstLevel(SecondLevel secondLevel) {
		this.secondLevel = secondLevel;
	}

	public SecondLevel getSecondLevel() {
		return secondLevel;
	}
}
