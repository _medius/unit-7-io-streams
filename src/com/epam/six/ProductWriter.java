package com.epam.six;

import com.epam.three.Product;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ProductWriter {

	private List<File> paths = new ArrayList<>();

	public List<File> serializeProducts(List<Product> products, String path) {
		for (Product product : products) {
			File filePath = new File(path + product.getTitle() + ".dat");

			try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(filePath))) {
				objectOutputStream.writeObject(product);
				paths.add(filePath);

				System.out.println(filePath.getName() + " has been written.");

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return paths;
	}
}
