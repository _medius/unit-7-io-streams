package com.epam.two;

public class MainForTwo {
	private static final String PATH_TO_FILE = "C:\\Users\\Medius\\Desktop\\IOTasksFile.txt";

	private void secondTask() {
		System.out.println("Second task output.");
		ReverseLineReader reverseLineReader = new ReverseLineReader(PATH_TO_FILE);
		reverseLineReader.readLIneFile();
	}

	private void appStart() {
		secondTask();
	}

	public static void main(String[] args) {
		new MainForTwo().appStart();
	}
}
