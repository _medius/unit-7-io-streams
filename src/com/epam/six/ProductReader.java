package com.epam.six;

import com.epam.three.Product;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ProductReader {

	private List<Product> products = new ArrayList<>();

	public List<Product> deserializeProducts(List<File> paths) {
		for (File filePath : paths) {
			try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(filePath))) {
				products.add((Product) objectInputStream.readObject());

				System.out.println(filePath.getName() + " has been read.");

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		return products;
	}
}
