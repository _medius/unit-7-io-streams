package com.epam.three;

import java.io.*;
import java.util.List;

public class DataWriter {

	private final String path;
	private final File filePath;

	public DataWriter(String path) {
		this.path = path;
		this.filePath = new File(path);
	}

	private void createFile() {
		try {
			filePath.delete();
			filePath.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeProducts(List<Product> products) {
		createFile();

		try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(filePath))) {
			for (Product product : products) {
				dataOutputStream.writeUTF(product.getTitle());
				dataOutputStream.writeFloat(product.getPrice());
				dataOutputStream.writeInt(product.getAmount());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("\nProducts has been written.");
	}
}
