package com.epam.three;

import java.util.ArrayList;
import java.util.List;

public class MainForThree {

	private static final String PATH_TO_FILE = "C:\\Users\\Medius\\Desktop\\thirdTaskFile.bin";

	private List<Product> products = new ArrayList<>();

	private void createData() {
		products.add(new Product("coat", 103.5f, 10));
		products.add(new Product("chair", 252.1f, 5));
		products.add(new Product("wrench", 25.3f, 3));
	}

	private void thirdTask() {
		System.out.println("Products to write:");
		products.forEach(System.out::println);

		DataWriter dataWriter = new DataWriter(PATH_TO_FILE);
		dataWriter.writeProducts(products);

		DataReader dataReader = new DataReader(PATH_TO_FILE);
		List<Product> readProducts = dataReader.readProducts();

		System.out.println("\nRead products:");
		readProducts.forEach(System.out::println);
	}

	private void appStart() {
		createData();
		thirdTask();
	}

	public static void main(String[] args) {
		new MainForThree().appStart();
	}
}
