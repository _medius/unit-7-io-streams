package com.epam.three;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataReader {

	private final String path;
	private final File filePath;
	private List<Product> result = new ArrayList<>();

	public DataReader(String path) {
		this.path = path;
		this.filePath = new File(path);
	}

	public List<Product> readProducts() {
		String title;
		float price;
		int amount;

		try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream(filePath))) {
			while (dataInputStream.available() > 0) {
				title = dataInputStream.readUTF();
				price = dataInputStream.readFloat();
				amount = dataInputStream.readInt();

				result.add(new Product(title, price, amount));
			}
		} catch (FileNotFoundException e ) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("\nProducts has been read.");
		return result;
	}
}
